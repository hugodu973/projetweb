-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le :  Dim 08 déc. 2019 à 22:54
-- Version du serveur :  5.7.24
-- Version de PHP :  7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `objetsv2`
--

-- --------------------------------------------------------

--
-- Structure de la table `obj`
--

CREATE TABLE `obj` (
  `Id` int(11) NOT NULL,
  `Nom` text NOT NULL,
  `URLImage` text NOT NULL,
  `Visible` tinyint(1) NOT NULL,
  `long` float NOT NULL,
  `lat` float NOT NULL,
  `Zoom` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `obj`
--

INSERT INTO `obj` (`Id`, `Nom`, `URLImage`, `Visible`, `long`, `lat`, `Zoom`) VALUES
(5, 'couple', 'images/le_couple.png', 1, -118.35, 34.1025, 11),
(6, 'Emir', 'images/emir.png', 1, 55.2967, 25.2694, 10),
(7, 'CoffreMercure', 'images/coffremercure.png', 1, 55.3167, 25.1594, 10),
(8, 'Courtier', 'images/le_courtier.png', 1, 0.000975, 51.5418, 11),
(9, 'Mercure', 'images/mercure.png', 0, 55.2467, 25.1594, 10),
(10, 'siteExtraction', 'images/zoneOrpaillage.jpg', 1, -54.019, 2.77828, 10),
(11, 'Or', 'images/or_brut.png', 0, -54.019, 2.89828, 10),
(12, 'DiamantBrut', 'images/diamand_brut.png', 0, -11.3999, 9.02057, 8),
(13, 'Bijoutier', 'images/bijoutier.png', 1, -118.335, 34.1025, 8),
(14, 'vieilAmi', 'images/vieilAmi.png', 1, 10.3793, 48.9599, 6),
(15, 'contremaitre', 'images/contremaitre1.png', 1, -11.5999, 9.01657, 9),
(16, 'mine', 'images/mine.png', 1, -11.3999, 9.01657, 10),
(17, 'pioche', 'images/pioche.png', 1, -11.3999, 9.21657, 10);

-- --------------------------------------------------------

--
-- Structure de la table `walloffame`
--

CREATE TABLE `walloffame` (
  `id` int(11) NOT NULL,
  `pseudo` text NOT NULL,
  `score` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `walloffame`
--

INSERT INTO `walloffame` (`id`, `pseudo`, `score`) VALUES
(8, 'Charles', 130),
(14, 'Hugo', 107),
(16, 'speedrun', 280);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `obj`
--
ALTER TABLE `obj`
  ADD PRIMARY KEY (`Id`);

--
-- Index pour la table `walloffame`
--
ALTER TABLE `walloffame`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `obj`
--
ALTER TABLE `obj`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT pour la table `walloffame`
--
ALTER TABLE `walloffame`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
