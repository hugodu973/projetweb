# ProjetWEB

Le site a été réalisé à l'aide de MAMP 4.1.1.18915, PHP 7.3.7 et MYSQL 5.7.24

1. Dans http://localhost/phpMyAdmin Créez une base de donnée nommé "objetsV2"
2. importez le fichier "objetsV2.sql" dans la base de donnée que vous venez de créer
3. Sur localhost, accéder au fichier "acceuil.html"


Solutions :

1. Trouver le courtier de Londres : cooordonnées : longitude 0.000975  lat 51.5418 
2. trouver le fournisseur à Dubai 55.2967 	25.2694
3. la solution de l'énigme est 2010 
4. zone d'extraction : coordonnées -54.019 	2.77828
5. cliquer sur le mercure dans l'inventaire, un halo blanc apparait, cliquer sur la zone d'extraction, l'or apparait un peu au nord, ramassez le 
6. retour vers le courtier
7. bijoutier : coordonnées -118.335 	34.1025 	
8. votre ami est au coordonnées 10.3793 	48.9599
9. le contremaitre de la mine de diamant est aux coordonnées -11.5999 	9.01657
10. cliquez sur la pioche au nord pour la ramasser, selectionner la dans l'inventaire et cliquez sur la mine, le diamant apparait, ramassez le
11. retournez voir le bijoutier