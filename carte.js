//
//INITIALISATION
//
var timeLeft = 360;
var mymap = L.map('mapid').setView([34.1024569, -118.3401521], 13);

document.getElementById("headerPseudo").innerHTML = localStorage.getItem("pseudo");
/*
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    maxZoom: 16,
    minZoom : 3,
    noWrap: true
}).addTo(mymap);
*/
/*
L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}', {
	attribution: 'Tiles &copy; Esri &mdash; Source: Esri, DeLorme, NAVTEQ, USGS, Intermap, iPC, NRCAN, Esri Japan, METI, Esri China (Hong Kong), Esri (Thailand), TomTom, 2012'
  maxZoom: 16,
  minZoom : 3,
  noWrap: true
}).addTo(mymap);
*/

L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/NatGeo_World_Map/MapServer/tile/{z}/{y}/{x}', {
    attribution: 'Tiles &copy; Esri &mdash; National Geographic, Esri, DeLorme, NAVTEQ, UNEP-WCMC, USGS, NASA, ESA, METI, NRCAN, GEBCO, NOAA, iPC',
    maxZoom: 16,
    minZoom: 3,
    noWrap: true
}).addTo(mymap);



mymap.addEventListener('zoomend', actualiseMarqueurZoom);

var objetSelectionne = null;
var dictIdToMarqueur = {};
var listeMarqueur = [];


list_id = [5, 6, 7, 8, 9, 10, 11, 12, 13,14,15,16,17];
for (var i = 0; i < list_id.length; i++) {
    createMarqueur(list_id[i]);
}

setTimeout(actualiseMarqueurZoom, 3000);

var dialogueCouple = ["Couple : Bonjour, nous souhaiterions acheter un bijou", "Bijoutier : Malheureusement chers client, nous n'avons plus de matière première. Mais je viens immédiatemment envoyer mon assistant en chercher !", "Bijoutier : *vous chuchotant* Depêche toi d'aller me chercher de quoi faire un bijou chez notre fournisseur"];
var dialogueBijoutier = ["Bijoutier : Bien, passe le moi... *le bijoutier l'examine puis se crispe brutalement*", "Bijoutier : On t'a refilé du toc, tu te moques de moi ? Depeche toi d'aller me trouver du diamant, et du vrai cette fois ci !"];
var dialogueVieilAmi = ["Ah mon ami ça me fait plaisir de te voir !", "Tu cherches du diamant ? J'ai une connaissance au Sierra Leone, dis lui que tu viens de ma part"];
var dialogueContremaitre = ["Tu veux extraire du diamant ? C'est bien parce que tu viens de sa part, mais tu devras l'extraire toi même"];
var dialogueBijoutier2 = ["Bijoutier : Enfin te voilà ! Vite donne moi le diamant !", "Bijoutier : un peu de patience chers clients, je vais immédiatemment vous confectionner votre bijou", "*Quelques dizaines de minute plus tard*", "Bijoutier : Et voilà monsieur, un magnifique bijou pour Madame !"];
var dialogueEmir = ["De l'or ? Je n'en ai plus... Mais j'ai le mercure nécesaire à son extraction. Vous pourrez ouvrir le coffre qui le contient si vous parvenez à résoudre mon énigme. Ecoutez donc...", "Emir : J'ai trois filles. Mon ainée est brune. Le produit de leurs âges donne 36. Leur somme donne 13. Quand est née la plus grande ?"];
var dialogueEmir2 = ["Vous avez trouvé la solution de l'énigme semble-t-il. Le mercure est à vous. Le site d'extraction est en Guyane"]
var dialogueCourtier = ["Courtier : Oui j'ai ce qu'il vous faut. Un magnifique diamant notamment. Mais comment comptez vous payer ? Je suis prêt à vous l'échanger contre de l'or,  allez mon associé à Dubai, au marché de l'or"];
var dialogueCourtier2 = ["Bien bien. Tenez voici votre diamant"];
var allObj = ["Va à Londres prendre contact avec le courtier", "Allez à Dubai trouver les producteurs", "Ouvrez le coffre à l'aide de l'énigme : 'Mon ainée est brune <br /> Le produit de leurs âges fait 36 <br /> La somme de leurs âges fait 13 <br /> En quelle année est née ma première fille ?", "Récupérer le mercure", "Parlez à l'Emir", "Utilisez le mercure sur le site d'extraction en Guyane", "Ramenez l'or au courtier", "Ramenez le diamant au bijoutier", "Allez trouver votre vieil ami mineur. Il est probablement en Europe actuellement", "Trouver les mines de diamants du Sierra Leone", "Extrayez du diamant","Ramenez le diamant au bijoutier","FIN"];
var indexObjEnCours = 0;

initDialogue(dialogueCouple);

setTimeout(function() {
    console.log(listeMarqueur);
    dictIdToMarqueur["8"].once("click", function() {
        initDialogue(dialogueCourtier)
    });
    dictIdToMarqueur["6"].once("click", function() {
        initDialogue(dialogueEmir)
    });
    dictIdToMarqueur["7"].on("click", createPopupCoffre);
    dictIdToMarqueur["10"].on("click", extractOr);
}, 5000)


launchCountdown();

var gagne = null;
//
//FIN INITIALISATION
//

// Gère la visibilitée des objets en fonction du zoom
function actualiseMarqueurZoom() {
    var currentZoom = mymap.getZoom();
    for (var i = 0; i < listeMarqueur.length; i++) {
        if (listeMarqueur[i]["options"]["icon"]["options"]["zoom"] > currentZoom) {
            listeMarqueur[i].remove();
        } else {
            if (listeMarqueur[i]["options"]["icon"]["options"]["visible"] == 1) {
                listeMarqueur[i].addTo(mymap);
            }
        }
    }
}

function loadOneObject(id) {
    var data = 'id=' + id;
    return fetch('objets.php', {
            method: 'post',
            body: data,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        })
        .then(r => r.json())
}

function createMarqueur(id) {
    return loadOneObject(id)
        .then(r => {
            console.log(1);
            var icon = L.icon({
                iconUrl: r["URLImage"],
                iconSize: [70, 75], // size of the icon
                iconAnchor: [22, 49], // point of the icon which will correspond to marker's location
                zoom: r["Zoom"],
                visible: r["Visible"]
                //popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
            });
            var marqueur = L.marker([r["lat"], r["long"]], {
                id: r["Id"],
                icon: icon
            })
            dictIdToMarqueur[r["Id"]] = marqueur;
            listeMarqueur.push(dictIdToMarqueur[r["Id"]]);
        })
}

//retire un marqueur de la carte pour l'ajouter à l'inventaire. Ne fait rien si l'inventaire est déjà plein
function ajouteInventaire(marqueur) {
    marqueur.remove();
    marqueur["options"]["icon"]["options"]["visible"] = 0;
    var limite_inventaire = 3;
    for (var i = 1; i < limite_inventaire+1; i++) {
        console.log("slot" + i);
        console.log(document.getElementById("slot" + i).innerHTML);
        console.log(marqueur["options"]["icon"]["options"]["iconUrl"]);
        var case_inventaire = document.getElementById("slot" + i);
        if (case_inventaire.innerHTML == "") {
            console.log('<img src="' + marqueur["options"]["icon"]["options"]["iconUrl"] + '" class="inventory_image obj' + marqueur["options"]["id"] + '" id="slot' + i + '" width="50" height="50">');
            case_inventaire.innerHTML = '<img src="' + marqueur["options"]["icon"]["options"]["iconUrl"] + '" class="inventory_image obj' + marqueur["options"]["id"] + '" id="obj' + i + '" width="50" height="50">';
            case_inventaire.addEventListener("click", function() {
                select_object(i);
            })
            break;
        }
    }
}

function removeInventaire(id) {
    var limite_inventaire = 3;
    for (var i = 1; i < limite_inventaire; i++) {
        var case_inventaire = document.getElementById("obj" + i);
        console.log(case_inventaire.className.split(" ")[1]);
        if (case_inventaire.className.split(" ")[1] == "obj" + id) {
            console.log("element a retirer trouvé");
            document.getElementById("slot" + i).innerHTML = "";
        }
    }
}

function select_object(n) {
    for (var i = 1; i < 3; i++) {
        var case_inventaire = document.getElementById("slot" + i);
        case_inventaire.style.boxShadow = "";
    }
    var case_inventaire = document.getElementById("slot" + n);
    case_inventaire.style.boxShadow = "5px 5px 5px white";
    var classe = document.getElementById("obj" + n).className;
    objetSelectionne = classe.split(" ")[1];
    console.log(classe);
    console.log(objetSelectionne);
}

function dialoguePart(allPhrases, i) {
    if (allPhrases.length != i) {
        var fenetre = document.getElementById("infoPanel");
        fenetre.innerHTML = allPhrases[i];
        mymap.once("click", function() {
            dialoguePart(allPhrases, i + 1)
        })
    } else if (gagne == true) {
        endGame(null);
    } else {
        afficheInfo();
        indexObjEnCours = indexObjEnCours + 1;

    }
}

function initDialogue(allPhrases) {
    console.log(allPhrases);
    dialoguePart(allPhrases, 0);
}

function afficheInfo() {
    var fenetre = document.getElementById("infoPanel");
    fenetre.innerHTML = "Objectif :" + allObj[indexObjEnCours];
}

function extractOr() {
    if (objetSelectionne == "obj9") {
        dictIdToMarqueur["10"].removeEventListener()
        dictIdToMarqueur["11"]["options"]["icon"]["options"]["visible"] = 1;
        actualiseMarqueurZoom();
        dictIdToMarqueur["11"].addEventListener("click", function() {
            afficheInfo();
            ajouteInventaire(dictIdToMarqueur["11"]);
            dictIdToMarqueur["8"].once("click", function() {
               indexObjEnCours = indexObjEnCours + 1;
               console.log("dialogue courtier");
               initDialogue(dialogueCourtier2);
                dictIdToMarqueur["12"]["options"]["icon"]["options"]["visible"] = 1;
                removeInventaire("11");
                dictIdToMarqueur["12"]["options"]["icon"]["options"]["visible"] = 1;
                ajouteInventaire(dictIdToMarqueur["12"]);
                dictIdToMarqueur["13"].once("click", function() {
                  initDialogue(dialogueBijoutier);
                  removeInventaire("12");
                    dictIdToMarqueur["14"].once("click", function() {
                      initDialogue(dialogueVieilAmi);
                      dictIdToMarqueur["15"].once("click", function() {
                        initDialogue(dialogueContremaitre);
                        //pioche
                        dictIdToMarqueur["17"].once("click", function() {ajouteInventaire(dictIdToMarqueur["17"])});
                        dictIdToMarqueur["16"].once("click", extractDiamant);
                      }
                    );
                  });

            })

        })
    });
  }
}

function extractDiamant(){
  console.log("enter extractdiam");
  if (objetSelectionne == "obj17"){
    console.log("ENTER THE IF");
    dictIdToMarqueur["12"]["options"]["icon"]["options"]["visible"] = 1;
    dictIdToMarqueur["12"].once("click", function() {
      ajouteInventaire(dictIdToMarqueur["12"]);
      afficheInfo();
      dictIdToMarqueur["13"].once("click", terminate);
    });

    //indexObjEnCours++;

  }
}
function createPopupCoffre() {
    var map = document.getElementById("backgroundBlocker");
    var div = document.createElement("div");
    div.innerHTML = "";
    div.innerHTML =
        '<div class="modal-content"> \
       <span  class="close">&times;</span>      \
        <form id = "formulaireCode">  \
        Entrez le code <br> \
          <input type="text" name="code" value=""><br> \
          <input type="submit" value="Submit"></form>   \
        </div>'
    map.appendChild(div);
    // Get the modal
    var modal = document.getElementById("backgroundBlocker");
    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];
    // When the user clicks on the button, open the modal
    backgroundBlocker.style.display = "block";


    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        backgroundBlocker.style.display = "none";
    }

    var form = document.getElementById("formulaireCode")
    form.addEventListener("submit", function() {
        testCode(event, form.elements["code"])
    });

    function testCode(e, code) {
        e.preventDefault();
        console.log(code.value);
        if (code.value == "2010") {
            span.remove();
            document.getElementById("formulaireCode").innerHTML = "<p>Le coffre s'ouvre... </p>";
            afficheInfo();
            dictIdToMarqueur["9"]["options"]["icon"]["options"]["visible"] = 1;
            dictIdToMarqueur["9"].once("click", function() {
                formulaireCode.remove();
                document.getElementsByClassName("modal-content")[0].remove();
                indexObjEnCours = indexObjEnCours + 1;
                afficheInfo();
                dictIdToMarqueur["6"].once("click", function() {
                    indexObjEnCours = indexObjEnCours + 1;
                    initDialogue(dialogueEmir2)
                })
                ajouteInventaire(dictIdToMarqueur["9"]);
            })
            actualiseMarqueurZoom();



        }
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            backgroundBlocker.style.display = "none";
        }
    }

}

function terminate() {
    gagne = true;
    initDialogue(dialogueBijoutier2);
}

function launchCountdown() {
    var interval = setInterval(function() {
        if (timeLeft == 0) {
            gagne = false;
            endGame(interval);
        } else {
            timeLeft--;
            minutes = Math.floor(timeLeft / 60);
            secondes = Math.floor(timeLeft - (minutes * 60));
            if (secondes < 10) {
                secondes = "0" + secondes;
            }
            document.getElementsByClassName("timer")[0].innerHTML = "<h1>TEMPS RESTANT : <br />" + minutes + ":" + secondes + "</h1>";
        }
    }, 1000)
}

function endGame(interval) {
    clearInterval(interval);
    var map = document.getElementById("backgroundBlocker");
    var div = document.createElement("div");
    div.innerHTML = "";
    if (gagne == false) {
        msg = '<p class = "endGame"> Perdu ! <br /> </p>';
    } else {
        msg = '<p class = "endGame"> Vous gagnez avec un score de ' + timeLeft + ' ! <br /> </p>';
        var data = "pseudo=" + localStorage.getItem('pseudo') + "&score=" + timeLeft + "&inserting==true";
        fetch('WallOfFame.php', {
            method: 'post',
            body: data,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        })
    }
    div.innerHTML =
        '<div class="modal-content">' + msg + '<button type="button"><a target="client" href="acceuil.html" > Rejouer !</a></button>  \
      </div>'
    map.appendChild(div);
    // Get the modal
    var modal = document.getElementById("backgroundBlocker");
    // Get the <span> element that closes the modal
    // When the user clicks on the button, open the modal
    backgroundBlocker.style.display = "block";
    window.onclick = function(event) {}
}
